#!/usr/bin/env bash

# get volume information
get_volume=$( amixer get Master| grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1 )
get_volume_muted=$( amixer get Master | grep '%' | grep -oE '[^ ]+$' | cut -d '[' -f 2 | cut -d ']' -f 1) 

# notification variable
time_msg="3000"
id_msg="312"
urg_msg="low"

# icons used in the script
icon_high="/usr/share/icons/Papirus-Dark/48x48/status/notification-audio-volume-high.svg"
icon_medium="/usr/share/icons/Papirus-Dark/48x48/status/notification-audio-volume-medium.svg"
icon_low="/usr/share/icons/Papirus-Dark/48x48/status/notification-audio-volume-low.svg"
icon_mute="/usr/share/icons/Papirus-Dark/48x48/status/notification-audio-volume-muted.svg"

# in what volume range the icons are used
if [ "$get_volume" -le "33" ]; then
    icon_vol=$( echo $icon_low )
elif [ "$get_volume" -le "66" ]; then
    icon_vol=$( echo $icon_medium )
else
    icon_vol=$( echo $icon_high )
fi

# volume bar configuration
bar_pattern="━"
bar_length=$(( $get_volume / 5 ))
printf -v output '%*s' "$bar_length"
bar=${output// /$bar_pattern}

dunst_msg=$(dunstify -u $urg_msg -r $id_msg -t $time_msg -i $icon_vol "  VOL" "   $bar")

case $1 in
    -u)
	# increase volume
	amixer set Master on > /dev/null
	amixer set Master $2%+ > /dev/null
	$dunst_msg
	;;
    -d)
	# decrease volume
	amixer set Master on > /dev/null
	amixer set Master $2%- > /dev/null
	$dunst_msg
	;;
    -m) 
	# mute volume
	amixer set Master toggle > /dev/null
	if [ "$get_volume_muted" == "on" ]; then
	    dunstify -u $urg_msg -r $id_msg -t $time_msg -i $icon_mute "  VOL" "  MUTE"
	else
	$dunst_msg
    fi
	;;
esac

