#!/usr/bin/env bash

#variables
urg_msg="low"

icon="/usr/share/icons/Papirus-Dark/48x48/apps/accessories-screenshot.svg"

case $1 in
    -f )
        scrot 'Screenshot_%Y-%m-%d_%H.%M.%S_full.png' -e 'mv $f ~/Dropbox/Screenshots'
        dunstify -u $urg_msg -i $icon "New Screenshot" "Fullscreen" 
    ;;
    -w )
        scrot 'Screenshot_%Y-%m-%d_%H.%M.%S_window.png' -bue 'mv $f ~/Dropbox/Screenshots'
        dunstify -u $urg_msg -i $icon "New Screenshot" "Window" 
    ;;
    -s )
        sleep 0.2
        scrot 'Screenshot_%Y-%m-%d_%H.%M.%S_select.png' -se 'mv $f ~/Dropbox/Screenshots'
        dunstify -u $urg_msg -i $icon "New Screenshot" "Select" 
    ;;
esac
