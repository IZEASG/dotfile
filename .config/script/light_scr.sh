#!/usr/bin/env bash

# get screen brightness as a rounded number
get_brightness=$( printf "%.0f\n" $( xbacklight ) )

# notification variables
time_msg="3000"
id_msg="313"
urg_msg="low"

# icons used in the script
icon_full="/usr/share/icons/Papirus-Dark/48x48/status/notification-display-brightness-full.svg"
icon_high="/usr/share/icons/Papirus-Dark/48x48/status/notification-display-brightness-high.svg"
icon_medium="/usr/share/icons/Papirus-Dark/48x48/status/notification-display-brightness-medium.svg"
icon_low="/usr/share/icons/Papirus-Dark/48x48/status/notification-display-brightness-low.svg"

# in what brightness range the icons are used
if [ "$get_brightness" -eq "100" ]; then
    icon_bright=$( echo $icon_full )
elif [ "$get_brightness" -ge "66" ]; then
    icon_bright=$( echo $icon_high )
elif [ "$get_brightness" -ge "33" ]; then
    icon_bright=$( echo $icon_medium )
else
    icon_bright=$( echo $icon_low )
fi

# brightness bar configuration
bar_pattern="━"
bar_length=$(( $get_brightness / 5 ))
printf -v output '%*s' "$bar_length"
bar=${output// /$bar_pattern}

dunst_msg=$( dunstify -u $urg_msg -r $id_msg -t $time_msg -i $icon_bright "  SCREEN" "   $bar" )

case $1 in
    -u)
	# increase brightness
	xbacklight -inc $2
	$dunst_msg
	;;
    -d)
        # decrease brightness
	xbacklight -dec $2
	$dunst_msg
        ;;
esac

