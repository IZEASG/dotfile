# path
export PATH="$PATH:$HOME/.config/script"
export PATH=~/.npm-global/bin:$PATH

# editor
export EDITOR=nvim

# browser
export BROWSER=/usr/bin/qutebrowser
