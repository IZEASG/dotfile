"------------------------------------------------------------------------------
"   Python options
"------------------------------------------------------------------------------
" set word wrapping to 79 characters
set textwidth=79

" display a ruler at the 80 character
set colorcolumn=80

" convert tabs into spaces
set expandtab
