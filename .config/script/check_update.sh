#!/usr/bin/env bash

# show notifications in cron
DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
export DBUS_SESSION_BUS_ADDRESS

# check for updates
pac=$( checkupdates | cut -d " " -f1 | wc -l )
aur=$( yay -Pua | cut -d " " -f1 | wc -l )

update=$(( $pac + $aur ))

printf -v update_sys "%02d" $update

# variables for dunstify
time_msg="6000"
id_msg="314"
urg_msg="low"
icon="/usr/share/icons/Papirus-Dark/24x24/actions/bqm-update.svg"

if [ $update -gt 1 ]; then
    dunstify -u $urg_msg -r $id_msg -t $time_msg -i $icon "New Updates" "$update_sys new updates."
elif [ $update -eq 1  ]; then
    dunstify -u $urg_msg -r $id_msg -t $time_msg -i $icon "New Update" "$update_sys new update."
fi
