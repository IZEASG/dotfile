"------------------------------------------------------------------------------
" TEX.vim
"------------------------------------------------------------------------------
filetype indent off

set colorcolumn=81
set textwidth=80

" spellcheck for all the file
let g:tex_nospell=0
let g:tex_comment_nospell=1

" enable syntax foldmethod
let g:tex_fold_enabled=1
set fdm=syntax

" save and compile .tex
" nnoremap <leader>m :w<CR>:!rubber --pdf --warn all %<CR>
nnoremap <leader>m :w<CR>:!latexmk -lualatex % && latexmk -c<CR>

" view generated pdf
nnoremap <leader>p :!zathura %:r.pdf &<CR><CR>
