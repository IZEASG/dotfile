# ~/.bashrc
#set -o vi
stty -ixon

# Import the colors.
[[ $- != *i* ]] && return

# alias
alias grep='grep --color=auto'
alias ls="ls -l --color=auto"
alias sl="ls -l --color=auto"
alias ñs="ls -l --color=auto"
alias ks="ls -l --color=auto"
alias la="ls -lhA"
alias al="ls -lhA"
alias ña="ls -lhA"
alias ka="ls -lhA"

alias diff='diff --color=auto'

alias df="df -h --total"    #file system disk space usage - human readeble
alias du="du -ach"

alias free="free -mt"
alias locate="locate -i"

alias ps="ps auxf"
alias psg="ps aux | grep -v grep | grep -i -e VSZ -e"

alias m="man"
alias t="touch"
alias s="sudo"
alias se="sudo -e"
alias cl="clear"
alias wg="wget -c"
alias hg="history | grep"
alias ka="killall"

alias mkdir="mkdir -pv"

alias cp="cp -r"
alias rm="rm -r"

alias syss="systemctl --type=service"
alias sys="sudo systemctl"

alias psys="cat /var/log/pacman.log | grep -iE 'installed|upgraded'"

# functions
fcd() {
    cd "$( fd . / --hidden --type d | fzf --height 15% )"
}

fe() {
    nvim "$( fd --hidden --type f | fzf --height 15% )"
}

fch() {
    cp "$( fd . '/home/gfv/' --hidden | fzf --height 15% )" $( pwd )
}

cpcd() { 
    num_files=$(( $# - 1 ))
    for i in $( eval echo {1..$num_files} )
    do
        eval cp \${$i} \${$#}
    done
    eval cd \${$#}
 }

mvcd() { 
    num_files=$(($# - 1))
    for i in $(eval echo {1..$num_files})
    do
        eval mv \${$i} \${$#}
    done
    eval cd \${$#}
 }

mkcd () {
    mkdir -p $1
    cd $1
}

extract () {
   if [ -f $1 ] ; then
       case $1 in
           *.tar.bz2)   tar xvjf $1    ;;
           *.tar.gz)    tar xvzf $1    ;;
           *.bz2)       bunzip2 $1     ;;
           *.rar)       unrar x $1     ;;
           *.gz)        gunzip $1      ;;
           *.tar)       tar xvf $1     ;;
           *.tbz2)      tar xvjf $1    ;;
           *.tgz)       tar xvzf $1    ;;
           *.zip)       unzip $1       ;;
           *.Z)         uncompress $1  ;;
           *.7z)        7z x $1        ;;
           *)           echo "don't know how to extract '$1'..." ;;
       esac
   else
       echo "'$1' is not a valid file!"
   fi
 }

c() {
    if [[ -e /usr/bin/bat ]]; then
        bat $1
    else
        cat $1
    fi
}

gc() {
    cd $( find ~/.config -maxdepth 3 -type d | fzf --height 15% )

    # if [[ -n "$1" ]]; then
    #     cd ~/.config && cd $1
    # else
    #     cd ~/.config
    # fi
}

# pacman
alias md="yay -Si"                  #pkg dependencies.
alias ms="yay"                      #pkg search.
alias mu="yay -Syu"                 #pkg update.
alias mi="yay -Qi"                  #pkg info.
alias mo="yay -Qtdq"                #pkg orphans.
alias mr="yay -Rsn"                 #pkg remove.
alias mR="yay -Rn"                  #pkg remove w/o dependencies.
alias ml="yay -Q"                   #pkg list.
alias mal="yay -Qm"                 #pkg aur list.
alias mel="yay -Qe"                 #pkg explicitly installed.
alias mul="yay -Pu"                 #pkg update list.

alias pcc="paccache -rk1"   #remove pacman cache - except the last one.
alias puc="paccache -ruk0"  #remove pacman cache - of uninstalled pkg.

# go to...
#alias <kb>="cd ~/.config/"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias gD="cd ~/Documents"
alias gd="cd ~/Downloads"
alias gb="cd ~/Dropbox"
alias gm="cd ~/Music"
alias gp="cd ~/Pictures"
alias gv="cd ~/Videos"

alias gsc="cd ~/.config/script"

# apps
alias e="$EDITOR"
alias we="weechat -d /home/gfv/.config/weechat/"

# git dotfile
alias dotfile="/usr/bin/git --git-dir=$HOME/.dotfile/ --work-tree=$HOME"

# get current branch in git repo
function parse_git_branch() {
	BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
	if [ ! "${BRANCH}" == "" ]
	then
		STAT=`parse_git_dirty`
		echo "[${BRANCH}${STAT}] "
	else
		echo ""
	fi
}

# get current status of git repo
function parse_git_dirty {
	status=`git status 2>&1 | tee`
	dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
	untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
	ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
	newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
	renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
	deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
	bits=''
	if [ "${renamed}" == "0" ]; then
		bits=">${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="*${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
    }

# prompt
# PS1='[\u@\h \W]\$ '

PS1="\[\033[1;32m\]\u\[\033[0;15m\]";
PS1+=" in ";
PS1+="\[\033[1;34m\]\W\[\033[0;15m\]";
PS1+="\[\033[38;5;9m\] \`parse_git_branch\`";
PS1+="\[\033[1;37m\]\\$ \[\033[0;15m\]";
export PS1;

export PS2="\[$(tput bold)\]\[\033[38;5;11m\]>>\[$(tput sgr0)\]"

export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
export PATH="$PATH:$(ruby -e 'puts Gem.user_dir')/bin"

# fuzzy
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

