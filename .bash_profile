#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

# editor
export EDITOR=nvim

# browser
export BROWSER=/usr/bin/qutebrowser
