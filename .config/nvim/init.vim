""-----------------------------------------------------------------------------
" VIMRC
""-----------------------------------------------------------------------------
""-----------------------------------------------------------------------------
" plugins
""-----------------------------------------------------------------------------
call plug#begin('~/.config/nvim/plugged')

" insert or delete brackets, parens, quotes in pair.
Plug 'jiangmiao/auto-pairs'
" plugin to colorize all text in the form #rrggbb or #rgb.
Plug 'lilydjwg/colorizer'
" Generate a beautiful color swatch for the current buffer
Plug 'cocopon/colorswatch.vim'
" fuzzy file, buffer, mru, tag, etc finder.
Plug 'ctrlpvim/ctrlp.vim'
" asynchronous completion framework.
if has('nvim')
    Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
    Plug 'Shougo/deoplete.nvim'
    Plug 'roxma/nvim-yarp'
    Plug 'roxma/vim-hug-neovim-rpc'
endif
" endwise.vim: wisely add "end" in ruby, endfunction/endif/more in vim script, etc
Plug 'tpope/vim-endwise'
" syntax for i3 window manager config.
Plug '$HOME/.config/nvim/plugged/i3-vim-syntax'
" plugin to display the indention levels with thin vertical lines.
Plug 'Yggdroot/indentLine'
" language server protocol support.
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }
" commentary.vim: comment stuff out
Plug 'tpope/vim-commentary'
" Template engine for creating Vim color scheme.
Plug 'cocopon/pgmnt.vim'
" automatically restoring file's cursor position and folding.
Plug 'vim-scripts/restore_view.vim'
" perform all your vim insert mode completions with tab.
Plug 'ervandew/supertab'
" plugin that displays tags in a window.
Plug 'majutsushi/tagbar'
" UltiSnips - The ultimate snippet solution for Vim
Plug 'SirVer/ultisnips'
" pandoc integration and utilities for vim.
Plug 'vim-pandoc/vim-pandoc'
" handles vim-pandoc's integration with third-party plugins.
Plug 'vim-pandoc/vim-pandoc-after'
" pandoc markdown syntax.
Plug 'vim-pandoc/vim-pandoc-syntax' 
" smart selection of the closest text object.
Plug 'gcmt/wildfire.vim'

call plug#end()

""-----------------------------------------------------------------------------
" general
""-----------------------------------------------------------------------------
" Encoding
set encoding=utf-8
set fileencoding=utf-8
set bomb
set binary
set ttyfast

set nocompatible
filetype plugin indent on
syntax enable

" backspace
set backspace=indent,eol,start

" where a new window appear
set splitright

" show command in the last line of the screen
set showcmd

" saves undo history to an undo file
set undofile
set history=500

" tabs/indent
set softtabstop=4		" number of spaces that a <TAB> counts
set shiftwidth=4		" number of spaces to use for autoindent
set expandtab			" number of spaces to insert a <TAB>

set autoindent			" copy indent from current line to new line

" command-line completion
set wildmenu			" command-line completion operates in an enhanced mode
set wildmode=longest:full,full	" completion mode that is used for the character specifed with wildcharm
set path+=**			" list of directories which will be searched when using commands

" list of file patterns to ignore when expanding wildcards
set wildignore+=*/.idea/*,*/.git/*,*/.hg/*,*/.svn/*,*/Videos/*,*/Music/*,*/Pictures/*,*/.offlineimap*/,*/tmp/*
set wildignore+=*/.DS_Store,*/vendor,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite,.swl,.swm,.swn,.swo,.swp

" set the <leader> key
let mapleader = "ç"

" hide inactive buffers
set hidden

" search options
set hlsearch			" highlight search results
set incsearch			" makes search act like search in modern browsers
set ignorecase			" ignore case when searching
set smartcase			" when searching try to be smart about cases

" directories path
set backupdir=~/.config/nvim/backup//
set directory=~/.config/nvim/swap//
set undodir=~/.config/nvim/undo//	" directory names for undo files

" detect changes in the file outside of vim
set autoread

" visual mode
set virtualedit=block

" wipe register
command! WipeReg for i in range(34,122) | silent! call setreg(nr2char(i), []) | endfor

" start terminal in insert mode
autocmd TermOpen,BufEnter term://* startinsert

" disable stuff in diff mode
if &diff
    syntax off
    let g:LanguageClient_autoStart = 0
endif

""-----------------------------------------------------------------------------
" interface settings
""-----------------------------------------------------------------------------
" colorscheme
colorscheme almost-monodark
set background=dark
set termguicolors

" show title in the window
set title
set titlestring=%(\ %{expand(\"%:~:.:F\")}%)\ %(\%a%)%(\%m%)%(\%r%)%(\%h%)%(\%w%)%(\%y%)

" Set 15 lines to the cursor - when moving vertically using j/k
set so=15

" show row and column ruler information
set ruler

" number
set number                  " show line number
set relativenumber          " show line number relative to cursor

set t_Co=256

" indentline
let g:indentLine_enabled = 1
let g:indentLine_concealcursor = 0
let g:indentLine_char = '┆'
let g:indentLine_faster = 1

" characters
" set list                  " display invisible characters
" display characters for invisible
set listchars=eol:¬,tab:▸·,trail:~,extends:>,precedes:<,space:·
" wrapped character
let &showbreak = '+++ '     " set character for wrapped lines
set cpoptions+=n            " show the wrapped characters in the number column

" cursor
set guicursor=              " keep cursor as square
set cursorline              " show cursorline

" fold
set foldcolumn=1

" remove statusline
set laststatus=0

" no vim in paste mode
set nopaste

""-----------------------------------------------------------------------------
" mapping
""-----------------------------------------------------------------------------
" save and quit
nnoremap <c-q> :q!<CR>
nnoremap <c-x> :wq<CR>
nnoremap <c-s> :w<CR>
nnoremap <c-a> ggVG

" clear highlight
nnoremap <silent><leader><Space> :nohlsearch<CR>

" tabs
nnoremap <silent><Tab> :tabnext<CR>
nnoremap <silent><S-Tab> :tabNext<CR>
nnoremap <silent><silent> tn :tabnew<CR>
nnoremap <silent><silent> td :tabclose<CR>

" move between lines when there is only soft wrapping
nnoremap <expr> j v:count ? 'j' : 'gj'
nnoremap <expr> k v:count ? 'k' : 'gk'

" vmap for maintain Visual Mode after shifting < and >
vmap < <gv
vmap > >gv

" create splits
nnoremap <silent><leader>h :<c-u>split<CR>
nnoremap <silent><leader>v :<c-u>vsplit<CR>

" move between split
tnoremap <a-h> <c-\><c-n><c-w>h
tnoremap <a-j> <c-\><c-n><c-w>j
tnoremap <a-k> <c-\><c-n><c-w>k
tnoremap <a-l> <c-\><c-n><c-w>l
inoremap <a-h> <c-\><c-n><c-w>h
inoremap <a-j> <c-\><c-n><c-w>j
onoremap <a-k> <c-\><c-n><c-w>k
inoremap <a-l> <c-\><c-n><c-w>l
nnoremap <a-h> <c-w>h
nnoremap <a-j> <c-w>j
nnoremap <a-k> <c-w>k
nnoremap <a-l> <c-w>l

nnoremap <silent><leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent><leader>- :exe "resize " . (winheight(0) * 2/3)<CR>
nnoremap <silent>+ :exe "vertical resize " . (winwidth(0) * 3/2)<CR>
nnoremap <silent>- :exe "vertical resize " . (winwidth(0) * 2/3)<CR>

nnoremap <silent><leader>_ <c-w>_<CR>
nnoremap <silent><leader>\| <c-w>\|<CR>

nnoremap <silent><leader>= <c-w>=<CR>

" escape key
inoremap jj <Esc>
inoremap kk <Esc>

" escape key terminal
tnoremap <Esc> <c-\><c-n>
tnoremap jj <c-\><c-n>
tnoremap kk <c-\><c-n>

" open terminal in a new tab
nnoremap <silent><leader>t :tabnew term://bash<CR>

" arrow keys
inoremap <silent><ESC>OA <UP>
inoremap <silent><ESC>OB <DOWN>
inoremap <silent><ESC>OC <RIGHT>
inoremap <silent><ESC>OD <LEFT>

nnoremap <Up> <NOP>
nnoremap <Down> <NOP>
nnoremap <Left> <NOP>
nnoremap <Right> <NOP>

vnoremap <Up> <NOP>
vnoremap <Down> <NOP>
vnoremap <Left> <NOP>
vnoremap <Right> <NOP>

" inoremap <Up> <NOP>
" inoremap <Down> <NOP>
" inoremap <Left> <NOP>
" inoremap <Right> <NOP>

" move lines up/down
nnoremap <silent><S-j> :m .+1<CR>==
nnoremap <silent><S-k> :m .-2<CR>==

" folding
nnoremap <silent><Space> @=(foldlevel('.')?'za':"\<Space>")<CR>
vnoremap <Space> zf

" focus in the current fold
nnoremap <leader>f zMzvzz

" invisible
nmap <silent><leader>i :set list!<CR>

" move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" set working directory
nnoremap <leader>. :lcd %:p:h<CR>

" opens an edit command with the path of the currently edited file filled in
noremap <silent><leader>e :e <C-R>=expand("%:p:h") . "/" <CR>

" opens a tab edit command with the path of the currently edited file filled
noremap <silent><leader><F2> :tabe <C-R>=expand("%:p:h") . "/" <CR>

" buffer
" nnoremap <silent> <leader>b :buffers<CR>:buffer<Space>
nnoremap <Leader>b :CtrlPBuffer<CR>

nnoremap <leader>n :bnext<cr>
nnoremap <leader>N :bprev<cr>

" spellcheck
map <F6> :set spell spelllang=es<CR>

" scratch/preview window
nnoremap <silent><leader>º <C-w>z

" keep search at the middle of the screen
nnoremap n nzzzv
nnoremap N Nzzzv

" don't move with *
nnoremap * *<c-o>

" open a quickfix window for the last search
nnoremap <silent><leader>/ :execute 'vimgrep /'.@/.'/g %'<CR>:copen<CR>

""-----------------------------------------------------------------------------
" plugin options
""-----------------------------------------------------------------------------
" ctrlp
let g:ctrlp_cmd = 'CtrlPMRU'
let g:ctrlp_match_window = 'bottom,order:btt,min:1,max:15,results:30'
let g:ctrlp_by_filename = 1
let g:ctrlp_regexp = 1
let g:ctrlp_show_hidden = 1
let g:ctrlp_clear_cache_on_exit = 0
let g:ctrlp_use_caching = 0
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
let g:ctrlp_user_command = 'fd --type f --hidden --color=never "" %s'

" deoplete
let g:deoplete#enable_at_startup = 1
let g:deoplete#max_abbr_width = 0
let g:deoplete#max_menu_width = 0

call deoplete#custom#source('_', 'sorters', ['sorter_word'])
call deoplete#custom#source('ultisnips', 'rank', 9999)
call deoplete#custom#option('min_pattern_length', 1)

" languageclient
let g:LanguageClient_serverCommands = {
    \ 'javascript': ['javascript-typescript-stdio'],
    \ 'python': ['pyls'],
    \ 'sh': ['bash-language-server', 'start'],
    \ 'c': ['ccls', '--log-file=/tmp/cc.log'],
    \ 'cpp': ['ccls', '--log-file=/tmp/cc.log'],
    \ 'cuda': ['ccls', '--log-file=/tmp/cc.log'],
    \ 'objc': ['ccls', '--log-file=/tmp/cc.log'],
    \ }


" netrw
let g:netrw_banner = 0
let g:netrw_browse_split = 4
let g:netrw_winsize = 25

map <F2> :Vex<CR>

" restore_view
set viewoptions=cursor,folds

" supertab
let g:SuperTabDefaultCompletionType = "<c-n>"

" Tagbar
let g:tagbar_autofocus = 1

nmap <silent> <F12> :TagbarToggle<CR>

" ultisnips
let g:UltiSnipsExpandTrigger = "<a-tab>"
let g:UltiSnipsListSnippets = "<c-l>"
let g:UltiSnipsJumpForwardTrigger = "<c-j>"
let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
let g:UltiSnipsEditSplit="vertical"

let g:UltiSnipsSnippetDirectories=[$HOME.'/.config/nvim/snips']

" vim-pandoc
let g:pandoc#filetypes#handled = ["pandoc", "markdown"]
let g:pandoc#filetypes#pandoc_markdown = 0
let g:pandoc#spell#enabled = 0

" vim-pandoc-after
let g:pandoc#after#modules#enabled = ["ultisnips", "supertab"]

